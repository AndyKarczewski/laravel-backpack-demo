# Andy's Laravel Backpack Demo

Laravel BackPack's demo, which includes all Backpack packages.


> ### Security updates and breaking changes
> Please **[subscribe to the Backpack Newsletter](http://eepurl.com/bUEGjf)** so you can find out about any security updates, breaking changes or major features. We send an email every 1-2 months.


![Example generated CRUD interface](https://dl.dropboxusercontent.com/u/2431352/backpack_base_login.png)


## Install

1) Run in your terminal:

``` bash
$ git clone git@bitbucket.org:AndyKarczewski/laravel-backpack-demo.git backpack-demo
```

2) Create your database, then set this database information in your .env file (use the .env.example as an example);

3) Run in your backpack-demo folder:
``` bash
composer install
php artisan key:generate
php artisan migrate
php artisan db:seed --class="Backpack\Settings\database\seeds\SettingsTableSeeder"
php artisan vendor:publish --provider="Backpack\CRUD\CrudServiceProvider" --tag="lang"
```

## Usage 

1. Register a new user at http://backpack-demo.dev/admin/register
2. Your admin panel will be available at http://backpack-demo.dev/admin
3. [optional] If you're building an admin panel, you should close the registration. In config/backpack/base.php look for "registration_open" and change it to false.

![Example generated CRUD interface](https://dl.dropboxusercontent.com/u/2431352/backpack_base_dashboard.png)

## Credits

- [Cristian Tabacitu][link-author]
- [All Contributors][link-contributors]

## License

Backpack is free for non-commercial use and $19/project for commercial use. Please see [License File](LICENSE.md) and [backpackforlaravel.com](https://backpackforlaravel.com/#pricing) for more information.

[link-author]: http://tabacitu.ro
[link-contributors]: ../../contributors