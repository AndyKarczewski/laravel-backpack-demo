<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\AttributeCrudRequest as StoreRequest;
use App\Http\Requests\AttributeCrudRequest as UpdateRequest;

class AttributeCrudController extends CrudController {

    public function setup() {
        $this->crud->setModel("App\Models\Attribute");
        $this->crud->setRoute("admin/attribute");
        $this->crud->setEntityNameStrings('attribute', 'attributes');

        $this->crud->setColumns(['name','article_id']);
        $this->crud->addField([
            'name' => 'name',
            'label' => "Attribute name"
        ]);
        $this->crud->addField([  // Select
           'label' => "Page Select",
           'type' => 'select',
           'name' => 'article_id', // the db column for the foreign key
           'entity' => 'article_id', // the method that defines the relationship in your Model
           'attribute' => 'name', // foreign key attribute that is shown to user
           'model' => "Backpack\PageManager\app\Models\Page" // foreign key model
        ]);
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
